include <subModules/common.scad>
include <subModules/arduino.scad>
include <subModules/lcdSupport.scad>

module pole(z_offeset = heightPlank, sideWidth = 5){
	color("yellow")
	translate([0,0,z_offeset])
	cube([20,20,sideWidth]);
}

module lowerPoles(){
	pole();
}

module lowerBase(){
	
	difference(){
        
        //main plan
		cube([lenghPlank,depthPlank,heightPlank]);
		translate([5,depthPlank / 10,0]){
			 arduinoBase();
		}
        translate([5,depthPlank / 2 + 5,0]){
			 plate();
		} 
	}
}
module topBase(){
	
		lcdSupport();
	difference(){
		cube([lenghPlank,depthPlank,heightPlank]);
		//remove space for lcd support
		translate([10,90,0]){
			cube([widthSupport,depthSupport,heightPlank]);
		}
	}
	
}

module main(){
	

	//Lower base and space for arduino
	lowerBase();
	//topBase();
	//TopBase with holes for LCDScreen and buttons
	//lcdSupport();
}	

 
main();