 
//Arduino plate Dimension 
lengthArduino = 81;
widthArduino  = 59; 

lenghtCircuitPlate = 70;
widthCircuitPalte = 50;



module plate(){

	color("blue") {
		translate([3,3,0]){
			holeForScrewArduinoBase(5);
		}        
		translate([3,widthCircuitPalte- 3,0]){
			holeForScrewArduinoBase(5);
		}
	}
}

module arduinoBase(){

	color("blue") {
		translate([3,3,0]){
			holeForScrewArduinoBase(5);
		}        
		translate([3,widthArduino- 3,0]){
			holeForScrewArduinoBase(5);
		}
		color("red")
		//hole for air
		translate([11,9,0]){
			cube([20,40,heightPlank]);
		}
	}
}

module holeForScrewArduinoBase(h = 3){
	cylinder(d=3,h);

}