module baseForArduinoUno(){

	lengthArduino = 68.6;
	widthArduino  = 53.4;
	marginForScrew = 1.6;
	heightArduino	= 4;

	color("purple")
	 	difference(){
			cube([lengthArduino,widthArduino,heightArduino]);
		
			translate([15,marginForScrew,0])
			holeForScrewArduinoBase(heightArduino);

			translate([15,widthArduino- marginForScrew,0])
				holeForScrewArduinoBase(heightArduino);

			translate([lengthArduino - marginForScrew,10,0])
				holeForScrewArduinoBase(heightArduino);


			translate([lengthArduino - marginForScrew,36,0])
				holeForScrewArduinoBase(heightArduino);
	 
		}
		
}
module holeForScrewArduinoBase(h = 3){

	cylinder(d=3,h);
}