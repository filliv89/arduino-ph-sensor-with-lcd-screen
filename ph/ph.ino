// include the library code:
#include <LiquidCrystal.h>
/*
Collegamento fili LCD

LCD  | ARDUINO PIN
1 | GND
2 | 5V
3 | USCITA POTEZIOMETRO
4 | 12
5 | GND
6 | 11
...
...
11  | 5
12  | 4
13  | 3
14  | 2
15  | 5V + 220hom
16  | GND
*/

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//PINS
const int sensorPin = A5;
const int brightnessLedPin = A0;
const int actionPumpPin = 10;
const int togglePhReadPin = 6;
const int manualPumpPin = 7;


//Dimension Lcd
const int columnMaxLcd = 16;
const int rowMaxLcd = 2;

//Costanti
const float noValuePh = -1;
const float correctionCoefficient = 2.8;  //Fix this in base of phSensor
const float phLimitPump = 7;

//global variabiles
float ph;
bool pumpActive;


void setup() {

  Serial.begin(9600);

  pinMode(brightnessLedPin, OUTPUT);
  pinMode(sensorPin, INPUT);
  pinMode(actionPumpPin, OUTPUT);
  pinMode(togglePhReadPin, INPUT);
  pinMode(manualPumpPin, INPUT);


  // set up the LCD's number of columns and rows:
  lcd.begin(columnMaxLcd, rowMaxLcd);
  lcd.clear();

  lcd.blink();
  delay(3000);
  lcd.noBlink();

  lcd.setCursor(0, 0);
    
  lcd.print("| Starting     | ");
  lcd.setCursor(0, 1);
  lcd.print("| Systems      | ");
  ph = 0;
  pumpActive = false;

  delay(2000);
}

float getPhpValueFromSensor() {

  int value = 0;
  float mV = 0;
  float ph = 0;

  //get AVG value after 10 reads
  for (int i = 0; i < 10; i++) {
    value += analogRead(sensorPin);
    delay(10);
  }
 
  value = value / 10.0;
  mV = (float)value * (5.0 / 1023) ;
  Serial.print("Mv ");
  Serial.print(mV);
  Serial.print(" ");
  
  
  ph = mV * correctionCoefficient;


  return ph;
}

bool IsPhBelowLimit(float ph) {

  return ph < phLimitPump;
}


char activePumpSimbol(int seconds,int columnCursor) {

  char simbol = '*';
  int mod = seconds % 2;
  lcd.setCursor(columnCursor, 1);
  switch (mod) {

    case 0:

      lcd.write('x');
      break;
    case 1:
      lcd.write('+');

      break;
  }

  return simbol;
}

void writeLcdSensorMessage(float ph, bool pumpActive) {

  Serial.print("ph ");
  Serial.println(ph);

  static unsigned long timer = 0;
  unsigned long interval = 1000;

  if (millis() - timer >= interval) {
    lcd.clear();
    timer = millis();
    int seconds = timer / 1000;

    //Set first line
    lcd.blink();
    
    lcd.setCursor(0, 0);
    lcd.print("pH Value: ");
    lcd.setCursor(11, 0);
    lcd.print(ph);

    //Set second line
    lcd.setCursor(0, 1);

    lcd.print("Pompa ");
    lcd.setCursor(8, 1);
    if (pumpActive) {
      activePumpSimbol(seconds,7);
      lcd.setCursor(8, 1);

      lcd.print("On");
    } else {

      lcd.print("Off");
    }
  }
}

bool isManualButtonPumpPressed() {

  return digitalRead(manualPumpPin);
}

void openCloseRele(bool pumpActive) {

  digitalWrite(actionPumpPin, pumpActive);
}

void writeLcdNoReadsMessage(bool manualButtonPressed) {

  static unsigned long timer = 0;
  unsigned long interval = 1000;

  if (millis() - timer >= interval) {
    lcd.clear();
    timer = millis();
    int seconds = timer / 1000;


    lcd.setCursor(0, 0);
    lcd.print("Sensor disabled");
    
  }
}

void sensorDisabled() {

  bool manualPumpPressed = isManualButtonPumpPressed();
  openCloseRele(manualPumpPressed);
  writeLcdNoReadsMessage(manualPumpPressed);
}
void sensorEnabled() {

  float ph = getPhpValueFromSensor();

  bool pumpActive = IsPhBelowLimit(ph);
  openCloseRele(pumpActive);

  writeLcdSensorMessage(ph, pumpActive);
}


void loop() {

  bool sensonEnabled =digitalRead(togglePhReadPin);
  
  if (sensonEnabled) {
    sensorEnabled();
  } else {
    sensorDisabled();
  }
}
