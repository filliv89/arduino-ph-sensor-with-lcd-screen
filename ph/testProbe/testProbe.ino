#include <Arduino.h>
// include the library code:
#include <LiquidCrystal.h>
const int adcPin = A5;
const float m = -2; 

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

const int brightnessLedPin = A0;
const int sensorPin = A5;
const int actionPumpPin = 10;
const int togglePhReadPin = 6;
const int manualPumpPin = 7;


//Dimension Lcd
const int columnMaxLcd = 16;
const int rowMaxLcd = 2;

//Costanti
const float noValuePh = -1;
const float correctionCoefficient = 2.6;  //Fix this in base of phSensor 
const float phLimitPump = 7;

//global variabiles
float ph;
bool pumpActive;


void setup() {
  Serial.begin(9600);
 
    pinMode(brightnessLedPin, OUTPUT); 
  pinMode(sensorPin, INPUT);
  pinMode(actionPumpPin, OUTPUT);
  pinMode(togglePhReadPin, INPUT);
  pinMode(manualPumpPin, INPUT);


  // set up the LCD's number of columns and rows:
  lcd.begin(columnMaxLcd, rowMaxLcd);
  lcd.clear();

  lcd.blink();
}

void loop() {

  
  int adcValue = 0; 
  int maxI = 10;
  for(int i =0; i< maxI; i++){
    adcValue+= analogRead(adcPin);
  }
  float avgAdcValue = adcValue / maxI; 

  float phVoltage = (float)avgAdcValue * 5.0 / 1024;
  Serial.print("ADC = ");
  Serial.print(avgAdcValue);
  Serial.print("  Po  = ");
  Serial.println(phVoltage, 3);
  
   float phValue = 7 - (2.5 - phVoltage) * m;
   Serial.print("p h value = "); 
   Serial.println(phValue);

  delay(1000);
}